<?php
/*
Template Name: Produits
*/
get_header();
?>

<main id="primary" class="site-main">
    <div class="container">
        <div class="row margin-btm">
        <div class="col-lg-7">
          <h2><?php the_content();?></h2>
        </div>
        </div>
        <div class="row margin-btm">
          <div class="col-lg-5 right offset-lg-1">
            <img class="speech" src="<?php echo get_template_directory_uri(); ?>/img/speech.png" alt="">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut
              morbi gravida orci. </p>
          </div>
          <div class="col-lg-5">
            <img class="speech" src="<?php echo get_template_directory_uri(); ?>/img/group.png" alt="">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut
              morbi gravida orci. </p>
          </div>
          <div class="col-lg-5 right offset-lg-1">
            <img class="speech" src="<?php echo get_template_directory_uri(); ?>/img/group2.png" alt="">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut
              morbi gravida orci. </p>
          </div>
          <div class="col-lg-5">
            <img class="speech" src="<?php echo get_template_directory_uri(); ?>/img/group3.png" alt="">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut
              morbi gravida orci. </p>
          </div>
        
      </div>

      <div class="row">
        <div class="col-lg-12 card--produit">
          <div class="container">
            <div class="row">
              <div class="col-lg-5">
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien
                  ut morbi gravida orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas
                  malesuada dolor aliquam sapien ut morbi gravida orc. Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci. Lorem ipsum dolor
                  sit amet, consectetur adipiscing elit. Nulla egestas malesuada dolor aliquam sapien ut morbi gravida
                  orc. </p>
                <button>J'achète !</button>
              </div>
              <div class="col-lg-7">
                <?php the_post_thumbnail( 'large', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px' ));?>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php 
get_footer();
