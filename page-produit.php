<?php
/*
Template Name: Produit
*/
get_header();
?>

<main id="primary" class="site-main">

<div class="container">
    <?php
    get_template_part("template-parts/produit/part", "produit");
    ?>
</div>

</main>

<?php 
get_footer();