<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ThemeEffix
 */

?>

	<footer id="colophon" class="site-footer">
			<div class="container">
				<div class="footer">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 flex">
								<img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo-effix-ads-black.png" alt="">
								<p class="address">52265 boul charest Est<br>Québec, Qc<br>G2B 3T8</p>
							</div>
							<div class="col-lg-6 right">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'link-footer',
										'menu_id'        => 'footer',
									)
							);
							?>
							<div>
								<img class="social" src="<?php echo get_template_directory_uri(); ?>/img/carbon-logo-twitter.png" alt="">
								<img class="social" src="<?php echo get_template_directory_uri(); ?>/img/bx-bxl-facebook-circle.png" alt="">
								<img class="social" src="<?php echo get_template_directory_uri(); ?>/img/bx-bxl-instagram-alt.png" alt="">
							</div>
							</div>
								<div class="line"></div>
							<div class="col-lg-12">
								<p>2020 ©<br>Fait avec love par Fire Team</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="site-info">
			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
