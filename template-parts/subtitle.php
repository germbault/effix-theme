<?php
/*
Template Name: Subtitle functionnality
Template Post Type: subtitle
*/
?>
    <div class="row margin-btm">

<?php
$subloop = new WP_Query(
    array(
        'post_type' => 'subtitle',
        'posts_per_page' => 1
    )
);
while ( $subloop->have_posts() ) : $subloop->the_post();
?>
        <div class="col-lg-5 col-sm-12">
            <h2><?php the_title();?></h2>
        </div>
        <div class="col-lg-1"></div>
        
<?php endwhile;
wp_reset_postdata();
?>
