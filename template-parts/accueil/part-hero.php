<?php
/*
Template Name: Intro
Template Post Type: accueil
*/
?>

<div class="container">
    <div class="row">

<?php
$homeloop = new WP_Query(
    array(
        'post_type' => 'accueil',
        'posts_per_page' => 3
    )
);
while ( $homeloop->have_posts() ) : $homeloop->the_post();
?>
        <div class="col-lg-5">
            <?php the_post_thumbnail()?>
        </div>
        <div class="col-6 offset-lg-1">
            <h2 class="right"><?php the_title();?></h2>
            <p class="right"><?php the_content();?></p>
            <div class="right">
                <button>Voir nos offres</button>
                <button class="reverse">Contactez-nous</button>
            </div>
        </div>
<?php endwhile;
wp_reset_postdata();
?>
    </div>
</div>