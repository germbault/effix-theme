<?php
/*
Template Name: Team
Template Post Type: team
*/
?>



<?php
$teamloop = new WP_Query(
    array(
        'post_type' => 'team',
        'posts_per_page' => 5
    )
);
while ( $teamloop->have_posts() ) : $teamloop->the_post();
?>    
<div class="">

<div class="row no-gutters profil_card">

  <div class="col-lg-6 col-sm-12 img">
      <?php the_post_thumbnail();?>
  </div>
    <div class="col-lg-4 align-self-center offset-lg-1">
      <h2>
      <?php the_title()?>
      </h2>
      <?php the_content();?>
    </div>

</div>

</div>
<?php endwhile;
wp_reset_postdata();
?>