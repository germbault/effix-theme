
<div class="row">
    <div class="col-lg-7">
        <h2><?php the_content(); ?></h2>
    </div>
    <?php
        $functloop = new WP_Query(
            array(
                'post_type' => 'functionnality',
                'posts_per_page' => 4,
            )
        );
        while ( $functloop->have_posts() ) : $functloop->the_post();
        ?>
                    <div class="col-lg-5 right offset-lg-1">
                        <?php the_post_thumbnail();?>
                        <?php the_content();?>
                    </div>
        <?php endwhile;
        wp_reset_postdata();
    ?>
</div>

<?php if (have_posts($post = 39)) : ?>
    <div class="row">
        <div class="col-lg-12 card--produit">
            <div class="container">
                <div class="row">
                    <?php
                            $functloop = new WP_Query(
                                array(
                                    'post_type' => 'product',
                                    'posts_per_page' => 1,
                                )
                            );
                            while ( $functloop->have_posts() ) : $functloop->the_post();
                                $le_id = get_the_ID();
                    ?>
                                <div  id="price_none" class="col-lg-5">
                                    <h2><?php the_title(); ?></h2>
                                    <?php the_content();
                                    echo do_shortcode("[add_to_cart id=\"$le_id\"]");
                                    ?>
                                </div>
                                <div class="col-lg-7">
                                    <?php the_post_thumbnail('large', array( 'sizes' => '(max-width:511px) 511px, (max-width:482px) 482px, 482px' ) ) ?>
                                </div>
                    <?php   endwhile;
                            wp_reset_postdata();
                    ?>


                </div>
            </div>
        </div>
    </div>
<?php 
    endif; wp_reset_postdata();
?>


    <?php
        get_template_part("template-parts/sign-up-form");
    ?>

</main>
