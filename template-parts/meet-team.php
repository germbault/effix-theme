<?php
/*
Template Name: Meet the team
Template Post Type: meet-team
*/
?>


    <div class="row margin-btm">

<?php
$meetloop = new WP_Query(
    array(
        'post_type' => 'meet-team',
        'posts_per_page' => 1
    )
);
while ( $meetloop->have_posts() ) : $meetloop->the_post();
?>
<div class="col-lg-12 col-sm-12">
          <div class="contact">
            <h3><?php the_title();?></h3>
            <div class="">
                <?php the_content();?>
            </div>
            <button class="reverse--white">Meet the team</button>
          </div>
        </div>
        
<?php endwhile;
wp_reset_postdata();
?>
    </div>