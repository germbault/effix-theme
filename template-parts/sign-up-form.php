<?php
/*
Template Name: Sign-up-form
Template Post Type: sign-up-form
*/
?>


    <div class="row margin-btm">

<?php
$signloop = new WP_Query(
    array(
        'post_type' => 'sign-up-form',
        'posts_per_page' => 1
    )
);
while ( $signloop->have_posts() ) : $signloop->the_post();
?>
        <div class="col-lg-12">
          <div class="contact">
            <h3><?php the_title();?></h3>

            <form action="#" class="mail-sign">
                <?php the_content();?>
            </form>
          </div>
        </div>
<?php endwhile;
wp_reset_postdata();
?>
    </div>
