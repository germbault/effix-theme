<?php
/*
Template Name: functionnality
Template Post Type: functionnality
*/
?>

<?php
$functloop = new WP_Query(
    array(
        'post_type'      => 'functionnality',
        'posts_per_page' => 2,
        'order'          => 'ASC'
    )
);
while ( $functloop->have_posts() ) : $functloop->the_post();
?>
        <div class="col-lg-3 col-sm-12">
          <div class="card">
            <h3><?php the_title();?></h3>
            <p><?php the_content();?></p>
          </div>
        </div>
<?php endwhile;
wp_reset_postdata();
?>
    </div>
