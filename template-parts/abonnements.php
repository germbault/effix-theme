<?php
/*
Template Name: Abonnement
Template Post Type: abonnement
*/
?>
<div class="row margin-btm">

    <?php
        $abmloop = new WP_Query(
            array(
                'post_type' => 'abonnement',
                'posts_per_page' => 1
            )
        );
        while ( $abmloop->have_posts() ) : $abmloop->the_post();
    ?>
        <div class="col-lg-5">
            <h2><?php the_title();?></h2>
            <p><?php the_content();?></p>
            <a href="<?php echo site_url();?>/produit"><button> En profiter</button></a>
            
        </div>
        <div class="col-lg-7 col-sm-12">
            <?php the_post_thumbnail( 'large', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px' ) );?>
        </div>
            
    <?php endwhile;
    wp_reset_postdata();
    ?>
</div>
