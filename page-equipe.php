<?php
/*
Template Name: Équipe
*/
get_header();
?>

<main id="primary" class="site-main">
    <div class="container">
        <div class="row margin-btm">
                <div class="col-lg-6 col-sm-12">
                    <?php the_content();?>
                </div>
        
<?php
get_template_part("template-parts/team");
get_template_part("template-parts/sign-up-form");
?>
        </div>
    </div>
</main>

<?php
get_footer();