<?php

/**
 * ThemeEffix functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ThemeEffix
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

if (!function_exists('themeeffix_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */

    function themeeffix_setup()
    {
        /*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ThemeEffix, use a find and replace
		 * to change 'themeeffix' to the name of your theme in all the template files.
		 */
        load_theme_textdomain('themeeffix', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
        add_theme_support('title-tag');

        /*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Primary', 'themeeffix'),
                'footer-link' => esc_html__('footer', 'themeeffix'),
            )
        );

        /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
            )
        );

        // Set up the WordPress core custom background feature.
        add_theme_support(
            'custom-background',
            apply_filters(
                'themeeffix_custom_background_args',
                array(
                    'default-color' => 'ffffff',
                    'default-image' => '',
                )
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'height'      => 250,
                'width'       => 250,
                'flex-width'  => true,
                'flex-height' => true,
            )
        );
    }
endif;
add_action('after_setup_theme', 'themeeffix_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function themeeffix_content_width()
{
    $GLOBALS['content_width'] = apply_filters('themeeffix_content_width', 640);
}
add_action('after_setup_theme', 'themeeffix_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function themeeffix_widgets_init()
{
    register_sidebar(
        array(
            'name'          => esc_html__('Sidebar', 'themeeffix'),
            'id'            => 'sidebar-1',
            'description'   => esc_html__('Add widgets here.', 'themeeffix'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action('widgets_init', 'themeeffix_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function themeeffix_scripts()
{
    wp_enqueue_style('themeeffix-style', get_stylesheet_uri(), array(), _S_VERSION);
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap-grid.min.css', array(), _S_VERSION);
    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css', array(), _S_VERSION);
    wp_enqueue_style('style', get_template_directory_uri() . '/css/woocommerce.css');
    wp_style_add_data('themeeffix-style', 'rtl', 'replace');

    wp_enqueue_script('themeeffix-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'themeeffix_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

function create_posttype()
{
    register_post_type(
        'subtitle',
        array(
            'labels' => array(
                'name' => __('Subtitle'),
                'singular_name' => __('Subtitle')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'subtitle'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );
    register_post_type(
        'functionnality',
        array(
            'labels' => array(
                'name' => __('Functionnality'),
                'singular_name' => __('Functionnality')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'functionnality'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );
    register_post_type(
        'sign-up-form',
        array(
            'labels' => array(
                'name' => __('Sign-up-form'),
                'singular_name' => __('Sign-up-form')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'sign-up-form'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );
    register_post_type(
        'abonnement',
        array(
            'labels' => array(
                'name' => __('Abonnement'),
                'singular_name' => __('Abonnement')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'abonnement'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );

    register_post_type(
        'meet-team',
        array(
            'labels' => array(
                'name' => __('Meet the team'),
                'singular_name' => __('Meet the team')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'meet-team'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );

    register_post_type(
        'team',
        array(
            'labels' => array(
                'name' => __('Team'),
                'singular_name' => __('Team')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'team'),
            'show_in_rest' => true,
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );
}


/**
 * Rend visible les utilisateurs qui n'ont pas fait de post.
 */
add_action('init', 'create_posttype');

add_filter('rest_user_query', 'custom_rest_user_query');
function custom_rest_user_query($prepared_args, $request = null)
{
    unset($prepared_args['has_published_posts']);
    return $prepared_args;
}
