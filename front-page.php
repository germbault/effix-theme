<!-- Page d'accueil -->
<?php
get_header();
?>

<main id="primary" class="site-main">
    <div class="container">
        <div class="row margin-btm">
            <div class="col-lg-5 col-sm-12">
                    <?php the_post_thumbnail( 'large', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px' ) );?>
                </div>
                <div class="col-lg-6 offset-lg-1 col-sm-12">
                    <?php the_content();?>
                    <div>
                        <button>Voir nos offres</button>
                        <button class="reverse">Contactez-nous</button>
                    </div>
                </div>
        </div>
<?php
get_template_part("template-parts/subtitle");
get_template_part("template-parts/functionnality");
get_template_part("template-parts/sign-up-form");
get_template_part("template-parts/abonnements");
get_template_part("template-parts/meet-team");
?>
        
    </div>
</main>

<?php 
get_footer();
